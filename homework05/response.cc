/*
  Implementation of methods for classes Response, AngryResponse, HappyResponse
*/
#include <iostream>
#include <string>
#include <algorithm>

#include "response.h"


#include <sstream>
using namespace std;

/*
  Implementation of Word class
  Don't worry too hard about these implementations.
  If you'd like to learn more about STL see: 
    https://www.geeksforgeeks.org/the-c-standard-template-library-stl/
*/
string Word::upper()
{
  string result(theWord);
  transform(result.begin(), result.end(), result.begin(), ::toupper);
  return result;
}

string Word::lower()
{
  string result(theWord);
  transform(result.begin(), result.end(), result.begin(), ::tolower);
  return result;
}

/*
  Implementation of Response methods
*/
bool Response::checkAndRespond(const string& inWord, ostream& toWhere)
{
    // This method should check if the current object's keyword is in the
    // input message (inWord), and send the proper response to the toWhere
    // output stream
    
    stringstream str_strm(inWord);
    string word;

    char delim = ' ';

    // iterate through inWord
    while (getline(str_strm, word, delim)) 
    {
	   
	   // check against keyword
	   long unsigned int counter = 0;
	   // iterate through word
	   // Special characters may be present, we don't strip because
	   // we're comparing against size of keyword.theWord therefore
	   // size of word will always be larger if special characters exist
	   // In other words, counter will reach this size before special characters
	   // are compared against (always come after word)
	   for (long unsigned int i = 0; i < word.length(); i++) 
	   {
		   if ((word[i] == (keyword.normal())[i]) or 
		       (word[i] == (keyword.upper())[i]) or
		       (word[i] == (keyword.lower())[i])) 
		   {
			   counter++;

		   }
	   }

	   // Comparing if all characters in word are present in keyword.theWord
	   if (counter == (keyword.normal()).length()) 
	   {
		   // respond to word
		   respond(toWhere);
		   return true;
	   }
    } 

    return false;

}

void Response::respond(ostream& toWhere)
{
    // TODO:
    // This method should 'insert' "I am neither angry nor happy: " followed by
    // the object's response word to the toWhere output stream, along with
    // a newline at the end
    toWhere << "I am neither angry nor happy: " << response << endl;

}


// TODO:
// Implement the 'respond' member function for the AngryResponse class so that
// the angry rseponse "I am angry: " followed by the object's response word 
// is inserted to the toWhere output stream, along with a newline at the end
/*
  Implementation of AngryReponse methods
*/
void AngryResponse::respond(ostream& toWhere)
{
	toWhere << "I am angry: " << response << endl;
}


// TODO:
// Implement the 'respond' member function for the HappyResponse class so that
// the happy rseponse "I am happy: " followed by the object's response word 
// is inserted to the toWhere output stream, along with a newline at the end
/*
  Implementation of HappyResponse methods
*/
void HappyResponse::respond(ostream& toWhere) 
{
	toWhere << "I am happy: " << response << endl;
}

