/* Michael O'Connell
 * CIS 330 C/C++ & Unix
 * Prof. Jee Choi
 * 1/20/20
 */
#include <stdlib.h>
#include <stdio.h>
#include "main.h"

/* Since the largest number is 4294967295, we need 12 characters (including the 
   null character) to read a number from the file */
#define MAX_NUM_LENGTH 12


/* This function checks the number of input parameters to the program to make 
   sure it is correct. If the number of input parameters is incorrect, it 
   prints out a message on how to properly use the program.
   input parameters:
       int    argc
       char** argv 
    return parameters:
       none
 */
void usage(int argc, char** argv)
{
    if(argc < 4) {
        fprintf(stderr, 
                "usage: %s <input file 1> <input file 2> <output file>\n", 
                argv[0]);
        exit(EXIT_FAILURE);
    }
}


/* This function takes in the two input file names (stored in argv) and
   determines the number of integers in each file.
   If the two files both have N integers, return N, otherwise return -1.
   If one or both of the files do not exist, it should exit with EXIT_FAILURE.
   input parameters:
       char** argv
   return parameters:
       -1 if the two input files have different number of integers 
       N  if the two input files both have N integers
 */
int get_num_ints(char** argv)
{
	/* If files do not exist, exit with EXIT_FAILURE */
	FILE* file_ptr1 = fopen(argv[1], "r");
       	FILE* file_ptr2 = fopen(argv[2], "r");
	
	// if either pointers are NULL (i.e one or both files do not exist)
	if(!(file_ptr1 && file_ptr2)) {
		fprintf(stderr, "Error: One or both input files do not exist.\n");
		exit(EXIT_FAILURE);
	}
	
	/* Determining the number of integers in each file */ 
	char line[MAX_NUM_LENGTH];
	unsigned long int file1_count, file2_count;
	file1_count = file2_count = 0;
	
	/* Because every integer is on a newline and
	 * each integer is a maximum of MAX_NUM_LENGTH
	 * loop is counting each integer until e.o.f
	 * (end of file) when that happens, loop terminates
	 * Because fgets() returns NULL
	 */
	while(fgets(line, MAX_NUM_LENGTH, file_ptr1)) {
		file1_count++;		
	}

	while(fgets(line, MAX_NUM_LENGTH, file_ptr2)) {
		file2_count++;
	}

	//closing files
	fclose(file_ptr1);
	fclose(file_ptr2);
	
	/* Comparing the amount of integers from each file */
	if(file1_count == file2_count) {
		//each file has the same amount of integers
		return file1_count;
	}
	else {
		return -1;
	}
}


/* This function allocates engough memory to the three arrays to store
   num_ints elements each.
   This function should exit with EXIT_FAILURE if the program fails to allocate
   the memory.
   input parameters:
       unsigned int*      input_one
       unsigned int*      input_two
       unsigned long int* output
       int                num_ints
   return parameters:
       none
 */
void allocate_mem(unsigned int** input_one, unsigned int** input_two, 
                  unsigned long int** output, int num_ints)
{
	/* Allocating memory and checking if program fails to allocate (malloc will return NULL if fails to allocate) */
	*input_one = (unsigned int*) malloc(sizeof(unsigned int) * num_ints);
	if(*input_one == NULL) {
		exit(EXIT_FAILURE);
	}

	*input_two = (unsigned int*) malloc(sizeof(unsigned int) * num_ints);
	if(*input_two == NULL) {
		exit(EXIT_FAILURE);
	}

	*output = (unsigned long int*) malloc(sizeof(unsigned long int) * num_ints);
	if(*output == NULL) {
		exit(EXIT_FAILURE);
	}

}


/* This function reads in num_ints integers from the two input files and 
   stores them in input_one (first input file) and input_two (second input
   file).
   If one or both of the files do not exist, it should exit with EXIT_FAILURE.
   input parameters:
       char**        argv
       unsigned int* input_one
       unsigned int* input_two
       int           num_ints
   return parameters:
       none

 */
void get_ints(char** argv, unsigned int* input_one, unsigned int* input_two,
              unsigned long int* output, int num_ints)
{
	
	/* If files do not exist, exit with EXIT_FAILURE */
	FILE* file_ptr1 = fopen(argv[1], "r");
       	FILE* file_ptr2 = fopen(argv[2], "r");
	
	// if either pointers are NULL (i.e one or both files do not exist)
	if(!(file_ptr1 && file_ptr2)) {
		fprintf(stderr, "Error: One or both input files do not exist.\n");
		exit(EXIT_FAILURE);
	}
		 
	unsigned int line1, line2;
	int i;

	for(i = 0; i < num_ints; i++) {
		//storing the value of each integer in files in line1 & line2
		fscanf(file_ptr1, "%u", &line1);
		fscanf(file_ptr2, "%u", &line2);

		//assigning array indices to file ints
		input_one[i] = line1;
		input_two[i] = line2;
	}	

	//closing files
	fclose(file_ptr1);
	fclose(file_ptr2);

}

/* This function does an element-wise addition between the two input arrays 
   (input_one and input_two) of size num_ints and stores the result in the 
   output array (output).
   input parameters:
       unsigned int*      intput_one
       unsigned int*      intput_two
       unsigned long int* output
       int                num_ints
   return parameters:
       none
 */
void sum_ints(unsigned int* input_one, unsigned int* input_two, 
              unsigned long int* output, int num_ints)
{
	int i;
	
	/* element-wise addition between the two input arrays, storing result in output array */
	for(i = 0; i < num_ints; i++) {
		// typecasting to unsigned long int to hold larger ranges of values
		output[i] = (unsigned long int) input_one[i] + input_two[i];
	}
}

/* This function saves the summed output to an output file, whose name was 
   specified with the program execution (i.e., argv[3]).
   The file should containe one integer value per line, similarly to how the
   input files were stored.
   input parameters:
       char**             argv
       unsigned int*      intput_one
       unsigned int*      intput_two
       unsigned long int* output
       int                num_ints
   return parameters:
       none
 */
void save_output(char** argv, unsigned int* input_one, unsigned int* input_two,
                 unsigned long int* output, int num_ints)
{
	
	/* If file does not exist, exit with EXIT_FAILURE */
	FILE* output_file_ptr = fopen(argv[3], "w");
	
	// if pointer is NULL (i.e file does not exist)
	if(!output_file_ptr) {
		fprintf(stderr, "Error: output file does not exist.\n");
		exit(EXIT_FAILURE);
	}

	/* Saving summed output to output file */
	int i;

	for(i = 0; i < num_ints; i++) {
		fprintf(output_file_ptr, "%lu\n", output[i]);
	}
	
	/* Closing output file */
	fclose(output_file_ptr);
}

/* This program takes in three text file names as input. 
   The first two files contain N integers (where N >=0 and N <= 1,000,000)
   whose values range from 0 to 4294967295 (i.e., 32-bit unsigned integers).
   The last file is the output file that this program should generate.
   For all three files, there should be one integer per line.
  
   For each line in the two input files, read in the two integers, add them, and
   then store the sum in the output file.
   Repeat this process until all integers have been read from the input files.

   For example, if the two input files have eight integerse each
   input file 1		input file 2		output file
   --------------------------------------------------------
   1			2			3
   5			2			7
   8			5			13
   1			12			13
   2			2			4
   10			2			12
   1991			2			1993
   11231245		21235			11252480
 */
int main(int argc, char** argv)
{
    usage(argc, argv);

    // Check the number of integers in the input files
    int num_ints = get_num_ints(argv);
   
    if(num_ints == -1) {
        fprintf(stderr, "ERR: The two input files have different # of ints\n");
        exit(EXIT_FAILURE);
    } else {
        fprintf(stdout, "The two input files have %d integers\n", num_ints);
    }

    unsigned int* input_one = NULL;
    unsigned int* input_two = NULL;
    unsigned long int* output = NULL;
    // Allocate memory to store the integers
    allocate_mem(&input_one, &input_two, &output, num_ints);

    // Read the integers from the two input files
    get_ints(argv, input_one, input_two, output, num_ints);
   
    // Sum up the numbers
    sum_ints(input_one, input_two, output, num_ints);
    for(int i = 0; i < num_ints; i++) printf("%ld\n", output[i]);

    // Store the result in the output file 
    save_output(argv, input_one, input_two, output, num_ints);

    free(input_one);
    free(input_two);
    free(output);

    return 0;

}
